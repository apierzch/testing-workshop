package pl.jug.warszawa.jinkubator.testing.cart;

import java.math.BigDecimal;

public interface PriceService {
    BigDecimal priceForProduct(Long productId);
}
