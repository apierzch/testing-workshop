package pl.jug.warszawa.jinkubator.testing._01_Layout;

public class Calculator {
    private String display;

    public String display() {
        return display;
    }

    public void pressNumber(Integer number) {
        display = number.toString();
    }
}
