package pl.jug.warszawa.jinkubator.testing._04_Exceptions;

import com.googlecode.catchexception.CatchException;
import com.googlecode.catchexception.apis.CatchExceptionAssertJ;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


public class Exceptions {

    @Test
    public void shouldCatchException() {
        // given
        List list = new ArrayList();

        // when
        CatchExceptionAssertJ.when(list).get(1);

        // then
        Exception exception = CatchException.caughtException();
        assertThat(exception)
                .isInstanceOf(IndexOutOfBoundsException.class)
                .hasMessageStartingWith("Index: 1");
    }
}
