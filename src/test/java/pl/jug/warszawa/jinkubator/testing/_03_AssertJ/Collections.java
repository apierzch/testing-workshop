package pl.jug.warszawa.jinkubator.testing._03_AssertJ;

import org.assertj.core.api.Condition;
import org.junit.Test;
import pl.jug.warszawa.jinkubator.testing._01_Layout.Calculator;

import java.util.Comparator;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static java.lang.Integer.valueOf;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

public class Collections {
    @Test
    public void contains() {
        Calculator calc1 = new Calculator();
        Calculator calc2 = new Calculator();

        List<Calculator> list = newArrayList();
        list.add(calc1);
        list.add(calc2);

        assertThat(list).contains(calc1);
        assertThat(list).containsOnly(calc2, calc1);
        assertThat(list).containsExactly(calc1, calc2);
        assertThat(list).doesNotContain(new Calculator());
        assertThat(calc1).isIn(list);
    }

    @Test
    public void custom() {
        Calculator calc1 = new Calculator();
        Calculator calc2 = new Calculator();
        calc1.pressNumber(1);
        calc2.pressNumber(2);

        assertThat(calc1).usingComparator((o1, o2) ->
            valueOf(o1.display().length()).compareTo(valueOf(o2.display().length()))
        ).isEqualTo(calc2);
    }

    @Test
    public void size() {
        assertThat(newArrayList()).isEmpty();
        assertThat(asList(1, 3, 5)).hasSize(3);
        assertThat(asList(1, 3, 5)).isNotEmpty();
        assertThat(asList(1, 3, 5)).hasSameSizeAs(asList(2, 4, 6));
    }

    @Test
    public void conditions() {
        assertThat(asList(1, 3, 5)).areAtLeast(1, new Condition<Integer>() {
            @Override
            public boolean matches(Integer value) {
                return value >= 3;
            }
        });
        assertThat(asList(1, 3, 5)).areAtMost(2, new Condition<Integer>() {
            @Override
            public boolean matches(Integer value) {
                return value >= 3;
            }
        });
        assertThat(asList(1, 3, 5)).areExactly(2, new Condition<Integer>() {
            @Override
            public boolean matches(Integer value) {
                return value >= 3;
            }
        });
        assertThat(asList(1, 3, 5)).areNot(new Condition<Integer>() {
            @Override
            public boolean matches(Integer value) {
                return value > 5;
            }
        });
    }

    @Test
    public void extracting() {
        Calculator calc = new Calculator();
        calc.pressNumber(1);

        assertThat(asList(calc)).extracting("display").contains("1").doesNotContain("5");
        assertThat(asList(calc)).extractingResultOf("display").contains("1").doesNotContain("5");
    }
}
