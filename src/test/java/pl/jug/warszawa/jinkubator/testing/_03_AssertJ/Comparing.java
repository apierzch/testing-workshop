package pl.jug.warszawa.jinkubator.testing._03_AssertJ;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class Comparing {

    @Test
    public void equal() {
        Integer actual = 1;
        Integer expected = 1;

        assertEquals(expected, actual);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void same() {
        Object first = new Object();
        Object second = first;

        assertSame(second, first);
        assertThat(first).isSameAs(second);
    }

    @Test
    public void instance() {
        List<?> actual = new ArrayList();

        assertThat(actual).isInstanceOf(ArrayList.class);
    }

}
