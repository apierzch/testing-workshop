package pl.jug.warszawa.jinkubator.testing._01_Layout;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CalculatorTest {

    @Test
    public void shouldDisplayNumbers() {
        // given
        Calculator calc = new Calculator();

        // when
        calc.pressNumber(1);

        // then
        assertThat(calc.display()).isEqualTo("1");
    }
}
